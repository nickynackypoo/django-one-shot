from django.contrib import admin
from .models import TodoList, TodoItem

class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

admin.site.register(TodoItem, TodoItemAdmin)
admin.site.register(TodoList, TodoListAdmin)
# Register your models here.
