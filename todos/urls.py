from django.urls import path
from .views import todo_list_list, todo_list_detail_view

urlpatterns = [
    path("", todo_list_list, name="todo-list"),
    path("<int:id>/", todo_list_detail_view, name="todo_list_detail")
]