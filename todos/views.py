from django.shortcuts import render, get_object_or_404
from .models import TodoList, TodoItem

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_lists": todo_list}
    return render(request, "todos/list.html", context)

def todo_list_detail_view(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {"todo_list_details": todo_list_detail}
    return render(request, "todos/detail.html", context)

